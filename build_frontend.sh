#!/usr/bin/env bash

rm -rf src/main/resources/static/*
cd ../dashboard
npm run build
cp -R build/ ../drinksmart-server/src/main/resources/static/

echo "Have fun pushing your new frontend :)"