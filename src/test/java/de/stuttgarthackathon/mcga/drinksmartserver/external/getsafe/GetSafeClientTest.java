package de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.stuttgarthackathon.mcga.drinksmartserver.entity.Drink;
import de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.domain.DrinksWrapper;
import de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.domain.EmailWrapper;
import de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.domain.TokenWrapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GetSafeClientTest {


  @Autowired
  private GetSafeClient getSafeClient;


  @Test
  @Ignore
  public void getFactsTtest() {
    Drink drink = new Drink();
    drink.setVolume(1123D);
    DrinksWrapper wrapper = new DrinksWrapper(Collections.singletonList(drink));
    getSafeClient.postFact(wrapper, "fake");
  }

  @Test
  public void getTokenTest() {
    String email = String.valueOf(System.currentTimeMillis()) + "@mcga.de";
    TokenWrapper wrapper = getSafeClient.getToken(new EmailWrapper(email));
    Assert.assertNotNull(wrapper.getToken());
  }

}
