package de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.stuttgarthackathon.mcga.drinksmartserver.entity.Bottle;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.Drink;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.Owner;
import de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.domain.DrinksWrapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GetSafeServiceTest {

  @Autowired
  private GetSafeService getSafeService;

  @Autowired
  private GetSafeClient getSafeClient;

  @Test
  @Ignore
  public void registerBottelTest() {
    Bottle bottle = registerTestBottle();
    Assert.assertNotNull(bottle.getOwner().getEmail());
    Assert.assertTrue(bottle.getOwner().getEmail().contains(bottle.getOwner().get_id().toHexString()));
    Assert.assertNotNull(bottle.getOwner().getToken());
    Assert.assertTrue(bottle.getOwner().getToken().startsWith("Token token"));
  }

  @Test
  @Ignore
  public void createDrinkEvent() {
    Bottle bottle = registerTestBottle();
    Drink drink = new Drink();
    drink.setVolume(100D);
    drink.setCreatedDate(Calendar.getInstance().getTime());
    bottle.setDrinks(Collections.singletonList(drink));
    getSafeService.createDrinkEvent(bottle);

    DrinksWrapper facts = getSafeClient.getFacts(bottle.getOwner().getToken());
    List<Drink> remoteDrinks = facts.getDrinks();
    Assert.assertTrue(remoteDrinks.get(0).getVolume().equals(drink.getVolume()));

  }


  private Bottle registerTestBottle() {
    Bottle bottle = new Bottle();
    bottle.setOwner(new Owner());

    return getSafeService.registerBottle(bottle);
  }
}
