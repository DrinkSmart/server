package de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit4.SpringRunner;

import feign.RequestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
@Profile("local")
public class GetSafeSecurityInterceptorTest {


  @Autowired
  private GetSafeSecurityInterceptor getSafeSecuritzInterceptor;


  @Test
  @Ignore
  // Real test, not execute without manual controll
  public void testAccessTokenReal() {
    RequestTemplate restTemplate = new RequestTemplate();
    getSafeSecuritzInterceptor.apply(restTemplate);
    Assert.assertTrue(restTemplate.headers().keySet().contains("Authorization"));
    Assert.assertTrue(restTemplate.headers().get("Authorization").stream().findFirst().get().startsWith("Token token"));
  }

}
