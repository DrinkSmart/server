package de.stuttgarthackathon.mcga.drinksmartserver.entity;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Document(collection = "bottles")
public class Bottle {

  @Id
  private String id;
  private List<Drink> drinks;
  private List<Refill> refills;
  private Owner owner;
  private Double totalDrinkAmount;
  private String publicKey;

  public Double getTotalDrinkAmount() {
    AtomicReference<Double> sum = new AtomicReference<>(0d);
    if (drinks != null) {
      drinks.forEach(it -> sum.updateAndGet(v -> v + it.getVolume()));
    }
    return sum.get();
  }
}
