package de.stuttgarthackathon.mcga.drinksmartserver.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Document(collection = "owners")
@JsonIgnoreProperties(value = {"_id"})
public class Owner {
  @Id
  private ObjectId _id;
  private String username;
  private String email;
  private String token;
  private int age;

  public Owner() {
    _id = ObjectId.get();
  }

  public String getId() {
    return _id.toString();
  }
}
