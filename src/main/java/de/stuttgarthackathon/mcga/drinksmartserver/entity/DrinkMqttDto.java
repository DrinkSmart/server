package de.stuttgarthackathon.mcga.drinksmartserver.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DrinkMqttDto {
  private String bottleId;
  private Double amount;
  private String unit;
}
