package de.stuttgarthackathon.mcga.drinksmartserver.entity;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Document(collection = "drinks")
@JsonIgnoreProperties(value = {"new", "createdDate"}, allowGetters = true)
public class Drink implements Persistable<String> {

  @Id
  private ObjectId _id;
  @CreatedDate
  private Date createdDate;
  private Double volume;
  private Double aggregate;

  public Drink() {
    _id = ObjectId.get();
  }

  public static Optional<Drink> from(DrinkMqttDto drinkMqttDto) {
    if (drinkMqttDto.getAmount() != null) {
      Drink drink = new Drink();
      drink.setVolume(drinkMqttDto.getAmount());
      return Optional.of(drink);
    }
    return Optional.empty();
  }

  public Double getAggregate() {
    return aggregate;
  }

  public void setAggregate(Double aggregate) {
    this.aggregate = aggregate;
  }

  @Override
  public String getId() {
    return _id.toString();
  }

  @Override
  @JsonIgnore
  public boolean isNew() {
    boolean isNew = volume == null;
    log.info("isNew: {}", isNew);
    if (isNew) {
      createdDate = Date.from(Instant.now());
      log.info("setting created Date: {}", createdDate);
    }
    return isNew;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Double getVolume() {
    return volume;
  }

  public void setVolume(Double volume) {
    isNew();
    this.volume = volume;
  }


}
