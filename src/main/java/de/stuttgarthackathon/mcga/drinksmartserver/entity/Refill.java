package de.stuttgarthackathon.mcga.drinksmartserver.entity;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Document(collection = "refills")
@JsonIgnoreProperties(value = {"new", "createdDate"}, allowGetters = true)
public class Refill implements Persistable<String> {

  @Id
  private ObjectId _id;
  @CreatedDate
  private Date createdDate;
  private Double volume;
  private Double price;
  private String currency;
  private String type;
  private String transactionId;

  public Refill() {
    _id = ObjectId.get();
  }

  public static Optional<Refill> from(DrinkMqttDto drinkMqttDto) {
    if (drinkMqttDto.getAmount() != null) {
      Refill drink = new Refill();
      drink.setVolume(drinkMqttDto.getAmount());
      return Optional.of(drink);
    }
    return Optional.empty();
  }


  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  @Override
  public String getId() {
    return _id.toString();
  }

  @Override
  @JsonIgnore
  public boolean isNew() {
    boolean isNew = volume == null;
    log.info("isNew: {}", isNew);
    if (isNew) {
      createdDate = Date.from(Instant.now());
      log.info("setting created Date: {}", createdDate);
    }
    return isNew;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Double getVolume() {
    return volume;
  }

  public void setVolume(Double volume) {
    isNew();
    this.volume = volume;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

}
