package de.stuttgarthackathon.mcga.drinksmartserver;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import de.stuttgarthackathon.mcga.drinksmartserver.entity.Bottle;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.Owner;
import de.stuttgarthackathon.mcga.drinksmartserver.repository.BottleRepository;

@SpringBootApplication
@EnableFeignClients
public class DrinksmartServerApplication implements CommandLineRunner {

  @Autowired
  BottleRepository bottleRepository;

  public static void main(String[] args) {
    SpringApplication.run(DrinksmartServerApplication.class, args);
  }

  @Bean
  public WebMvcConfigurer corsConfigurer() {
    return new WebMvcConfigurer() {
      @Override
      public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/api/**").allowedOrigins("*");
      }
    };
  }

  @Override
  public void run(String... args) throws Exception {
    initData();
  }

  private void initData() {
    if (!bottleRepository.findById("911").isPresent()) {
      Bottle bottle = new Bottle();
      Owner owner = new Owner();
      owner.setAge(30);
      owner.setEmail("herbert@mcga.de");
      owner.setUsername("TurboHerbert");
      bottle.setOwner(owner);
      bottle.setId("911");
      bottle.setDrinks(new ArrayList<>());

      bottleRepository.save(bottle);
    }
  }
}
