package de.stuttgarthackathon.mcga.drinksmartserver.controller.mqtt;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.stuttgarthackathon.mcga.drinksmartserver.entity.Drink;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.DrinkMqttDto;
import de.stuttgarthackathon.mcga.drinksmartserver.repository.BottleRepositoryUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DrinkSmartMqttController {

  private ObjectMapper objectMapper = new ObjectMapper();

  @Autowired
  private BottleRepositoryUtils bottleRepositoryUtils;

  private MqttAsyncClient asyncClient;

  @Value(value = "${mqtt.clientId}")
  private String mqttClientId;

  @Value(value = "${mqtt.serverUrl}")
  private String mqttServerUrl;

  @Value(value = "${mqtt.topic}")
  private String mqttTopic;


  private MqttConnectOptions mqttOptions() {
    MqttConnectOptions options = new MqttConnectOptions();
    options.setAutomaticReconnect(true);
    options.setCleanSession(true);
    options.setConnectionTimeout(20);
    return options;
  }

  @PostConstruct
  private void setup() throws MqttException {
    log.info("Trying to connect to MQTT Broker - serverUrl: {}, clientId: {}, topic: {}", mqttServerUrl, mqttClientId, mqttTopic);
    asyncClient = new MqttAsyncClient(mqttServerUrl, mqttClientId);
    MqttConnectOptions options = mqttOptions();
    asyncClient.connect(options, null, new IMqttActionListener() {
      @Override
      public void onSuccess(IMqttToken asyncActionToken) {
        log.info("onSuccess: Successfully connected to MQTT Broker");
        try {
          asyncClient.subscribe(mqttTopic, 1, (topic, message) -> {
            log.info("Inbound MQTT message - topic: {}, message: {}", topic, message.toString());
            if (topic.equalsIgnoreCase(mqttTopic)) {
              onHandleIncomingMessage(message);
            }
          });
        } catch (MqttException e) {
          e.printStackTrace();
        }
      }

      @Override
      public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        log.info("onFailure: Could not connect to MQTT Broker - cause: {}", exception.getMessage());
      }
    });

  }

  private void onHandleIncomingMessage(MqttMessage message) {
    try {
      DrinkMqttDto drinkMqttDto = objectMapper.readValue(message.toString(), DrinkMqttDto.class);
      Drink.from(drinkMqttDto).ifPresent(drink -> bottleRepositoryUtils.addDrinkToBottle(drinkMqttDto.getBottleId(), drink));
    } catch (IOException e) {
      log.info("Could not deserialize incoming MQTT Message to add a Drink - message: {}", message.toString());
      e.printStackTrace();
    }
  }

  @PreDestroy
  private void onUnsubscribe() throws MqttException {
    log.info("Unregistering MQTT Client");
    asyncClient.unsubscribe(mqttTopic);
  }

}
