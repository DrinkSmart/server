package de.stuttgarthackathon.mcga.drinksmartserver.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.stuttgarthackathon.mcga.drinksmartserver.controller.exception.BottleNotFoundException;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.Bottle;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.Drink;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.Refill;
import de.stuttgarthackathon.mcga.drinksmartserver.repository.BottleRepositoryUtils;

@RequestMapping("/api")
@RestController
@CrossOrigin(origins = "*")
public class DrinksController {

  private BottleRepositoryUtils bottleRepositoryUtils;

  @Autowired
  public DrinksController(BottleRepositoryUtils bottleRepositoryUtils) {
    this.bottleRepositoryUtils = bottleRepositoryUtils;
  }

  @PostMapping(path = "/bottles/{bottleId}/drinks", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public Bottle postDrink(@PathVariable(name = "bottleId") String bottleId, @RequestBody Drink drink) {

    Optional<Bottle> mutatedBottle = bottleRepositoryUtils.addDrinkToBottle(bottleId, drink);

    if (mutatedBottle.isPresent()) {
      return mutatedBottle.get();
    }

    throw new BottleNotFoundException("Bottle not found.");
  }

  @PostMapping(path = "/bottles/{bottleId}/refills", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public Bottle postDrink(@PathVariable(name = "bottleId") String bottleId, @RequestBody Refill refill) {

    Optional<Bottle> mutatedBottle = bottleRepositoryUtils.addRefillToBottle(bottleId, refill);

    if (mutatedBottle.isPresent()) {
      return mutatedBottle.get();
    }

    throw new BottleNotFoundException("Bottle not found.");
  }


}
