package de.stuttgarthackathon.mcga.drinksmartserver.controller.exception;

import java.util.Optional;

import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

@ControllerAdvice @RequestMapping(produces = "application/vnd.error+json")
public class DrinksExceptionsHandler {

  private ResponseEntity<VndErrors> error(final Exception exception, final HttpStatus httpStatus, final String logRef) {
    final String message = Optional.of(exception.getMessage()).orElse(exception.getClass().getSimpleName());
    return new ResponseEntity<>(new VndErrors(logRef, message), httpStatus);
  }

  @ExceptionHandler(BottleNotFoundException.class)
  public ResponseEntity<VndErrors> bottleNotFoundError(final BottleNotFoundException e) {
    return error(e, HttpStatus.NOT_FOUND, e.getLocalizedMessage());
  }
}
