package de.stuttgarthackathon.mcga.drinksmartserver.controller.exception;

public class BottleNotFoundException extends RuntimeException {

  public BottleNotFoundException(String message) {
    super(message);
  }

  BottleNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }
}
