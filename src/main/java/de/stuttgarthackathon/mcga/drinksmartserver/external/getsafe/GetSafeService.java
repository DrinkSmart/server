package de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe;

import de.stuttgarthackathon.mcga.drinksmartserver.entity.Bottle;

public interface GetSafeService {

  public Bottle registerBottle(Bottle bottle);

  public void createDrinkEvent(Bottle bottle);

}
