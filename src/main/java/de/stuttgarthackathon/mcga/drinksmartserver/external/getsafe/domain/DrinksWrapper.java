package de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.stuttgarthackathon.mcga.drinksmartserver.entity.Drink;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DrinksWrapper {

  private List<Drink> drinks;


}
