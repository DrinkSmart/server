package de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@Deprecated
public class GetSafeSecurityInterceptor implements RequestInterceptor {

  @Value("${getsafe.users.url}")
  private String getSafeUrl;

  @Value("${getsafe.users.email}")
  private String getSafeEmail;

  @Value("${getsafe.users.password}")
  private String getSafePassword;

  private static String accessToken = null;

  @Override
  public void apply(RequestTemplate template) {
    if (accessToken == null) {
      try {
        accessToken = getAccessToken();
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }
    template.header("Authorization", "Token token=\"" + accessToken + "\"");
  }

  private String getAccessToken() throws JSONException {
    RestTemplate restTemplate = new RestTemplate();
    MultiValueMap<String, String> requestMap = new LinkedMultiValueMap<String, String>();
    requestMap.add("email", getSafeEmail);
    requestMap.add("password", getSafePassword);
    String responseMap = restTemplate.postForObject(getSafeUrl + "/auth/login/email", requestMap, String.class);
    JSONObject json = new JSONObject(responseMap);
    return json.getString("token");
  }


  private String getNewAccessToken(String email) throws JSONException {
    RestTemplate restTemplate = new RestTemplate();
    MultiValueMap<String, String> requestMap = new LinkedMultiValueMap<String, String>();
    requestMap.add("email", email);
    String responseMap = restTemplate.postForObject(getSafeUrl + "/auth/registration/email", requestMap, String.class);
    JSONObject json = new JSONObject(responseMap);
    return json.getString("token");
  }
}
