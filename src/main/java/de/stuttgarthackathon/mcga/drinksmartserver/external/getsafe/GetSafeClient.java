package de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.domain.DrinksWrapper;
import de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.domain.EmailWrapper;
import de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.domain.TokenWrapper;


@FeignClient(name = "getSafeClient", url = "${getsafe.users.url}")
public interface GetSafeClient {

  @RequestMapping(method = RequestMethod.POST, value = "/me/facts")
  void postFact(@RequestBody DrinksWrapper drink, @RequestHeader("Authorization") String token);

  @RequestMapping(method = RequestMethod.GET, value = "/me/facts/latest")
  DrinksWrapper getFacts(@RequestHeader("Authorization") String token);

  @RequestMapping(method = RequestMethod.POST, value = "/auth/registration/email")
  TokenWrapper getToken(@RequestBody EmailWrapper email);

}
