package de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe;

import java.util.UUID;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import de.stuttgarthackathon.mcga.drinksmartserver.entity.Bottle;
import de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.domain.DrinksWrapper;
import de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.domain.EmailWrapper;
import de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.domain.TokenWrapper;

@Service
public class GetSafeServiceImpl implements GetSafeService {

  @Autowired
  private GetSafeClient getSafeClient;

  @Value("${getsafe.users.url}")
  private String getSafeUrl;

  @Override
  public Bottle registerBottle(Bottle bottle) {
    String email;
    String password;
    email = StringUtils.replace(bottle.getOwner().getUsername(), " ", "") + UUID.randomUUID().toString() + "@mcga.de";
    TokenWrapper tokenWrapper = getSafeClient.getToken(new EmailWrapper(email));
    password = "Token token=\"" + tokenWrapper.getToken() + "\"";
    bottle.getOwner().setEmail(email);
    bottle.getOwner().setToken(password);
    return bottle;
  }

  @Override
  public void createDrinkEvent(Bottle bottle) {
    getSafeClient.postFact(new DrinksWrapper(bottle.getDrinks()), bottle.getOwner().getToken());
  }

}
