package de.stuttgarthackathon.mcga.drinksmartserver.config;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

import de.stuttgarthackathon.mcga.drinksmartserver.entity.Bottle;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.Drink;

@Configuration
public class BottleRepositoryRestMvcConfiguration extends RepositoryRestMvcConfiguration {

  public BottleRepositoryRestMvcConfiguration(ApplicationContext context,
      ObjectFactory<ConversionService> conversionService) {
    super(context, conversionService);
  }

  @Override
  public RepositoryRestConfiguration repositoryRestConfiguration() {
    RepositoryRestConfiguration repositoryRestConfiguration = super.repositoryRestConfiguration();
    repositoryRestConfiguration.exposeIdsFor(Drink.class, Bottle.class);
    repositoryRestConfiguration.setBasePath("/api");
    return repositoryRestConfiguration;
  }
}