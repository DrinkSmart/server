package de.stuttgarthackathon.mcga.drinksmartserver.repository;

import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bigchaindb.model.Transaction;

import de.stuttgarthackathon.mcga.drinksmartserver.blockchain.BigChainDBDrinkSmartService;
import de.stuttgarthackathon.mcga.drinksmartserver.blockchain.keyCache;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.Bottle;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.Drink;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.Refill;

@Component
public class BottleRepositoryUtils {

  private BottleRepository bottleRepository;

  private BigChainDBDrinkSmartService bigChainService;

  @Autowired
  public BottleRepositoryUtils(BottleRepository bottleRepository, BigChainDBDrinkSmartService bigChainService) {
    this.bottleRepository = bottleRepository;
    this.bigChainService = bigChainService;
  }

  public Optional<Bottle> addDrinkToBottle(String bottleId, Drink drink) {
    Optional<Bottle> requestedBottle = bottleRepository.findById(bottleId);

    if (requestedBottle.isPresent() && drink.getVolume() > 0) {
      if (requestedBottle.get().getDrinks() == null) {
        requestedBottle.get().setDrinks(new ArrayList<>());
      }
      requestedBottle.get().getDrinks().add(drink);
      drink.setAggregate(aggregate(requestedBottle.get().getDrinks()));
      bottleRepository.save(requestedBottle.get());

      return requestedBottle;
    }
    return Optional.empty();
  }

  public Optional<Bottle> addRefillToBottle(String bottleId, Refill refill) {
    Optional<Bottle> requestedBottle = bottleRepository.findById(bottleId);

    if (requestedBottle.isPresent() && refill.getVolume() > 0) {
      if (requestedBottle.get().getRefills() == null) {
        requestedBottle.get().setRefills(new ArrayList<>());
      }

      String transaction = bigChainService.createRefill(refill);

      KeyPair key = keyCache.keymap.get(requestedBottle.get());
      if (key == null) {
        key = bigChainService.getKeys();
        keyCache.keymap.put(requestedBottle.get(), key);
      }

      Transaction transferTransaction = bigChainService.transferRefillmentToBottle(transaction, key);

      refill.setTransactionId(transferTransaction.getId());
      requestedBottle.get().getRefills().add(refill);
      requestedBottle.get().setPublicKey(transferTransaction.getOutputs().iterator().next().getPublicKeys().iterator().next());

      bottleRepository.save(requestedBottle.get());


      return requestedBottle;
    }
    return Optional.empty();
  }

  private Double aggregate(List<Drink> drinks) {
    AtomicReference<Double> sum = new AtomicReference<>(0d);
    if (drinks != null) {
      drinks.forEach(it -> sum.updateAndGet(v -> v + it.getVolume()));
    }
    return sum.get();
  }
}
