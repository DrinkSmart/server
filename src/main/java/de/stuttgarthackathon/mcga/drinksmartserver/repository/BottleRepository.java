package de.stuttgarthackathon.mcga.drinksmartserver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import de.stuttgarthackathon.mcga.drinksmartserver.entity.Bottle;

@RepositoryRestResource(collectionResourceRel = "bottles", path = "bottles")
public interface BottleRepository extends MongoRepository<Bottle, String> {

}
