package de.stuttgarthackathon.mcga.drinksmartserver.repository;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.stereotype.Component;

import de.stuttgarthackathon.mcga.drinksmartserver.entity.Bottle;
import de.stuttgarthackathon.mcga.drinksmartserver.entity.Owner;
import de.stuttgarthackathon.mcga.drinksmartserver.external.getsafe.GetSafeService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class BottleRepositoryEventListener extends AbstractMongoEventListener {

  private final GetSafeService getSafeService;
  private BottleRepository bottleRepository;

  @Autowired
  public BottleRepositoryEventListener(GetSafeService getSafeService, BottleRepository bottleRepository) {
    this.getSafeService = getSafeService;
    this.bottleRepository = bottleRepository;
  }

  @Override
  public void onAfterSave(AfterSaveEvent event) {
    log.info("called onAfterSave: {}", event);
    if (event.getSource() instanceof Bottle) {
      Bottle mutatedBottle = (Bottle) event.getSource();
      if (!hasCredentials(mutatedBottle)) {
        mutatedBottle = getSafeService.registerBottle(mutatedBottle);
        bottleRepository.save(mutatedBottle);
      } else {
        getSafeService.createDrinkEvent(mutatedBottle);
      }
    }
  }

  private boolean hasCredentials(Bottle bottle) {
    Owner bottleOwner = bottle.getOwner();
    return StringUtils.isNotBlank(bottleOwner.getToken()) && StringUtils.isNotBlank(bottleOwner.getEmail());
  }
}
