package de.stuttgarthackathon.mcga.drinksmartserver.blockchain;

import java.security.KeyPair;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeoutException;

import org.springframework.stereotype.Service;

import com.bigchaindb.builders.BigchainDbConfigBuilder;
import com.bigchaindb.builders.BigchainDbTransactionBuilder;
import com.bigchaindb.constants.Operations;
import com.bigchaindb.model.FulFill;
import com.bigchaindb.model.GenericCallback;
import com.bigchaindb.model.Transaction;

import de.stuttgarthackathon.mcga.drinksmartserver.entity.Refill;
import net.i2p.crypto.eddsa.EdDSAPrivateKey;
import net.i2p.crypto.eddsa.EdDSAPublicKey;
import okhttp3.Response;

/**
 * simple usage of BigchainDB Java driver (https://github.com/bigchaindb/java-bigchaindb-driver) to create TXs on BigchainDB network
 * 
 * @author dev@bigchaindb.com
 *
 */
@Service
public class BigChainDBDrinkSmartService {


  private final KeyPair refillerKey;

  public BigChainDBDrinkSmartService() {
    super();
    setConfig();
    refillerKey = getKeys();

  }


  private void onSuccess(Response response) {
    // TODO : Add your logic here with response from server
    System.out.println("Transaction posted successfully");
  }

  private void onFailure() {
    // TODO : Add your logic here
    System.out.println("Transaction failed");
  }

  private GenericCallback handleServerResponse() {
    // define callback methods to verify response from BigchainDBServer
    GenericCallback callback = new GenericCallback() {

      @Override
      public void transactionMalformed(Response response) {
        System.out.println("malformed " + response.message());
        onFailure();
      }

      @Override
      public void pushedSuccessfully(Response response) {
        System.out.println("pushedSuccessfully");
        onSuccess(response);
      }

      @Override
      public void otherError(Response response) {
        System.out.println("otherError" + response.message());
        onFailure();
      }
    };

    return callback;
  }


  /**
   * configures connection url and credentials
   */
  public static void setConfig() {
    BigchainDbConfigBuilder.baseUrl("https://test.bigchaindb.com/") // or use http://testnet.bigchaindb.com
        .addToken("app_id", "").addToken("app_key", "").setup();

  }

  /**
   * generates EdDSA keypair to sign and verify transactions
   * 
   * @return KeyPair
   */
  public static KeyPair getKeys() {
    // prepare your keys
    net.i2p.crypto.eddsa.KeyPairGenerator edDsaKpg = new net.i2p.crypto.eddsa.KeyPairGenerator();
    KeyPair keyPair = edDsaKpg.generateKeyPair();
    System.out.println("(*) Keys Generated..");
    return keyPair;

  }

  /**
   * performs CREATE transactions on BigchainDB network
   * 
   * @param assetData data to store as asset
   * @param metaData data to store as metadata
   * @param RefillerKey keys to sign and verify transaction
   * @return id of CREATED asset
   */
  public String createRefill(Refill refill) {

    // build and send CREATE transaction
    Transaction transaction = null;

    try {
      transaction = BigchainDbTransactionBuilder.init().addAssets(refill, Refill.class).operation(Operations.CREATE)
          .buildAndSign((EdDSAPublicKey) refillerKey.getPublic(), (EdDSAPrivateKey) refillerKey.getPrivate()).sendTransaction(handleServerResponse());
    } catch (TimeoutException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    System.out.println("(*) CREATE Transaction sent.. - " + transaction.getId());
    return transaction.getId();

  }

  /**
   * performs TRANSFER operations on CREATED assets
   * 
   * @param txId id of transaction/asset
   * @param metaData data to append for this transaction
   * @param bottleKey keys to sign and verify transactions
   */
  public Transaction transferRefillmentToBottle(String txId, KeyPair bottleKey) {

    Map<String, String> assetData = new TreeMap<String, String>();
    assetData.put("id", txId);


    // which transaction you want to fulfill?
    FulFill fulfill = new FulFill();
    fulfill.setOutputIndex(0);
    fulfill.setTransactionId(txId);

    // build and send TRANSFER transaction
    Transaction transaction = null;
    try {
      // build and send TRANSFER transaction
      transaction = BigchainDbTransactionBuilder.init().addInput(null, fulfill, (EdDSAPublicKey) this.refillerKey.getPublic())
          .addOutput("1", (EdDSAPublicKey) bottleKey.getPublic()).addAssets(txId, String.class).operation(Operations.TRANSFER)
          .buildAndSign((EdDSAPublicKey) this.refillerKey.getPublic(), (EdDSAPrivateKey) this.refillerKey.getPrivate())
          .sendTransaction(handleServerResponse());

      System.out.println("(*) TRANSFER Transaction sent.. - " + transaction.getId());
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return transaction;


  }
}
